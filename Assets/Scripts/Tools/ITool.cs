public interface ITool
{
    public void EnableTool();
    public void DisableTool();
    public bool IsDisableCompleted();
}