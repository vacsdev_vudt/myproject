﻿using System.Threading;
using UnityEngine;

public class Tool4 : MonoBehaviour, ITool
{
    private Thread enableThread;
    private Thread disableThread;

    private bool isDisableCompleted = true;

    public void EnableTool()
    {
        enableThread = new Thread(EnableToolThread);
        enableThread.Start();
    }

    public void DisableTool()
    {
        disableThread = new Thread(DisableToolThread);
        disableThread.Start();
    }

    private void EnableToolThread()
    {
        Debug.Log("Enable Tool 4!");
        isDisableCompleted = false;
    }

    private void DisableToolThread()
    {
        Thread.Sleep(1000);
        Debug.Log("Disable Tool 4!");

        // Khi luồng hoàn thành, đánh dấu rằng DisableTool đã thực hiện xong
        isDisableCompleted = true;
    }

    public bool IsDisableCompleted()
    {
        return isDisableCompleted;
    }
}