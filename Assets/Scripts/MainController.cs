using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour
{
    [SerializeField] List<ToolButton> toolButtons;

    void Start()
    {
        foreach (ToolButton tb in toolButtons)
        {
            tb.button.onClick.AddListener(() =>
            {
                if (tb.tool.GetComponent<ITool>().IsDisableCompleted()) enableTool(tb.tool);
                else tb.tool.GetComponent<ITool>().DisableTool();
            });
        }
    }

    private void Update()
    {
        Debug.Log("Update!!");
    }

    void enableTool(GameObject tool)
    {
        foreach (ToolButton tb in toolButtons)
        {
            if(tb.tool.GetComponent<ITool>().IsDisableCompleted() == false)
                tb.tool.GetComponent<ITool>().DisableTool();
        }

        StartCoroutine(enableToolWhenDisabledAllTool(tool));
    }

    IEnumerator enableToolWhenDisabledAllTool(GameObject tool)
    {
        yield return new WaitForSeconds(0f);

        if (checkAllToolIsDisable() == true) tool.GetComponent<ITool>().EnableTool();
        else StartCoroutine(enableToolWhenDisabledAllTool(tool));
    }

    bool checkAllToolIsDisable()
    {
        foreach (ToolButton tb in toolButtons)
        {
            if (!tb.tool.GetComponent<ITool>().IsDisableCompleted()) return false;
        }
        return true;
    }
}