﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable] // Đánh dấu lớp này để có thể hiển thị trong Inspector
public class ToolButton
{
    public Button button;
    public GameObject tool;
}